library orchestrator;

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart' as DotEnv;
import 'package:subapps_loader/main.dart' as subappsLoader;
import 'main_app.dart';
import 'package:authentication/authentication.dart';

void main({String initialRoute = '/'}) async {
  String mainAppRoute = initialRoute;
  if (kDebugMode) {
    try {
      await DotEnv.load();

      final routeFromEnv = DotEnv.env['INITIAL_ROUTE'];
      if (routeFromEnv != null && routeFromEnv.isNotEmpty) {
        mainAppRoute = routeFromEnv ?? mainAppRoute;
      }
    } catch (e) {
      // Maybe we don't have the .env because we will not need it (eg. using methodChannel only)
      // That's why we don't need to handle any errors, just print it on console
      print(e);
    }
  }

  final subApps = await subappsLoader.loadSubapps();

  //Preload
  WidgetsFlutterBinding.ensureInitialized();

  /// Instancia o mainApp
  MainApp mainApp = MainApp(
    subApps: subApps,
    route: mainAppRoute,
  );

  MyApp myApp = MyApp(
    subAppMap: mainApp.subApps,
    publicRoutes: mainApp.publicRoutes,
    mainNavigatorKey: mainApp.mainNavigatorKey,
    mainApp: mainApp,
    initialRoute: mainApp.route,
  );

  /// Inicializa instancias locais
  await mainApp.initializeInstances();

  /// Inicializa todos os subapps
  mainApp.initializeSubApps();

  runApp(MultiBlocProvider(providers: [
    BlocProvider(
        create: (BuildContext context) =>
            AuthenticationBloc(AuthenticationState.unknown()))
  ], child: myApp));
}
