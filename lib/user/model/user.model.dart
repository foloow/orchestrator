import 'package:json_annotation/json_annotation.dart';

part 'user.model.g.dart';

@JsonSerializable()
class User {
  const User({
    this.id,
    this.email,
    this.name,
  });

  final String id;
  final String email;
  final String name;

  static const empty = User(email: "", name: "", id: "");

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  Map<String, dynamic> toJson() => _$UserToJson(this);
}
