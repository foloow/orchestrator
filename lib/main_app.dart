import 'package:feed/main.dart';
import 'package:flutter/material.dart';
import 'package:foloow_local_storage/foloow_local_storage.dart';
import 'package:microapp/microapp.dart';
import 'package:tagger/tagger.dart';
import 'package:foloow_http_client/foloow_http_client.dart';

class MainApp {
  final String route;
  final Map<String, dynamic> headers;

  /// Map responsável por identificar os subapps
  Map<String, MicroApp> subApps;

  /// Map para registro de subapps
  Map<String, MicroAppRegistration> registrationMap;

  /// Rotas públicas
  Map<String, WidgetBuilder> publicRoutes;

  /// Key que será utilizada para navegação entre telas para toda a aplicação
  GlobalKey<NavigatorState> mainNavigatorKey = new GlobalKey<NavigatorState>();

  /// Instância de tageamento de dados
  Tagger tagger = new Tagger();

  /// Local Storage Instance
  FoloowStorage storage = new FoloowStorage();

  /// Http Instance
  FoloowHttp http = new FoloowHttp();

  /// Registro dos subapps
  Map<String, MicroAppRegistration> _registerSubApps() {
    return subApps.map((name, app) => new MapEntry(name, app.register()));
  }

  /// Carregar as rotas, passando por cada subapp e adicionando as rotas dos mesmos
  Map<String, WidgetBuilder> _loadPublicRoutes() {
    Map<String, WidgetBuilder> routes = {'/': (_) => FeedPage()};
    this.registrationMap.forEach(
        (name, registration) => routes.addAll(registration.publicRoutes));
    return routes;
  }

  Future<void> initializeInstances() async {
    /// Inicialiaz a instancia do storage
    await storage.init("foloow");
  }

  /// Inicialização de todos os subapps e onde é passado os paramêtros que serão recuperados nos subapps
  void initializeSubApps() {
    MicroAppInitializationParameters parameters =
        new MicroAppInitializationParameters(
            this.mainNavigatorKey, this.http, this.tagger, this.storage);
    this.subApps.forEach((name, app) => app.initialize(parameters));
  }

  /// Função para chamar os meodos de registro de subapp e carregamento de rotas
  MainApp({this.subApps, this.route, this.headers}) {
    this.registrationMap = _registerSubApps();
    this.publicRoutes = _loadPublicRoutes();
  }
}

class NoAnimationMaterialPageRoute<T> extends MaterialPageRoute<T> {
  NoAnimationMaterialPageRoute({
    @required WidgetBuilder builder,
    RouteSettings settings,
    bool maintainState = true,
    bool fullscreenDialog = false,
  }) : super(
            builder: builder,
            maintainState: maintainState,
            settings: settings,
            fullscreenDialog: fullscreenDialog);

  @override
  Widget buildTransitions(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation, Widget child) {
    return child;
  }
}

class Router {
  static Route<dynamic> generateRoute(
      {@required RouteSettings settings,
      @required Map<String, WidgetBuilder> routes,
      bool isInitialRoute = false}) {
    Uri uri = Uri.parse(settings.name);
    final routePathSegments = uri.pathSegments;

    var routeName = "/" + routePathSegments[0];
    var privateRoute = '/';

    if (!routes.keys.contains(routeName) && routePathSegments.length > 1) {
      final routeRootPath = routePathSegments.first;
      if (routes.keys.contains(routeRootPath)) {
        routeName = routeRootPath;
        privateRoute = '/${uri.pathSegments.sublist(1).join('/')}';
      }
    }
    if (routes.keys.contains(routeName)) {
      if (isInitialRoute) {
        return NoAnimationMaterialPageRoute(
            // builder: (context) => routes[routeName](context),
            builder: (context) => routes[routeName](context),
            maintainState: false,
            fullscreenDialog: false,
            settings: RouteSettings(
                name: privateRoute, arguments: uri.queryParametersAll));
      } else {
        return MaterialPageRoute(
            builder: (context) => routes[routeName](context),
            maintainState: false,
            fullscreenDialog: false,
            settings: RouteSettings(
                name: privateRoute, arguments: uri.queryParametersAll));
      }
    }
  }
}

/// Widget "pai" onde será inicializada a aplicação com as rotas e a navegação
class MyApp extends StatefulWidget {
  final Map<String, MicroApp> subAppMap;
  final Map<String, WidgetBuilder> publicRoutes;
  final GlobalKey<NavigatorState> mainNavigatorKey;
  final MainApp mainApp;
  final String initialRoute;

  MyApp({
    Key key,
    this.subAppMap,
    this.publicRoutes,
    this.mainNavigatorKey,
    this.mainApp,
    this.initialRoute,
  }) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> with WidgetsBindingObserver {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Main",
      routes: widget.publicRoutes,
      navigatorKey: widget.mainNavigatorKey,
      initialRoute: widget.initialRoute,
      theme: ThemeData(primarySwatch: Colors.blue),
      onGenerateRoute: (settings) {
        return Router.generateRoute(
            settings: settings, routes: widget.publicRoutes);
      },
      onGenerateInitialRoutes: (String initialRouteName) {
        return [
          Router.generateRoute(
              settings: RouteSettings(name: initialRouteName, arguments: null),
              routes: widget.publicRoutes,
              isInitialRoute: true)
        ];
      },
      onUnknownRoute: (settings) => MaterialPageRoute(builder: (context) {
        return Scaffold(
            backgroundColor: Colors.white,
            body: Container(
              child: Text('Unknown Route!'),
            ));
      }),
    );
  }
}
